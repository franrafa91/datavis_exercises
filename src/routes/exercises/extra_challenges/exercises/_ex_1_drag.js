import {drag} from 'd3-drag'
import {select} from 'd3-selection'

export function network_drag(x,simulation) {
  //console.log("Calling network_drag")
  //console.log(simulation)

  function dragsubject(event) {
    return simulation.find(event.x, event.y)
  }

  function onDragStart(event,d) {
    if (!event.active) {
     simulation.alphaTarget(0.3).restart();
      simulation.restart()
    }
    event.subject.fx = event.x;
    event.subject.fy = event.y;
   }
   
    function onDrag(event,d) {
    event.subject.fx = event.x;
    event.subject.fy = event.y;
   }

   function onDragEnd(event,d) {
    if (!event.active) {
     simulation.alphaTarget(0);
    }
    event.subject.fx = null;
    event.subject.fy = null;
   }
 
    select(x).call(drag().subject(dragsubject)
    .on("start", onDragStart)
    .on("drag", onDrag)
    .on("end", onDragEnd));  
}
