import { select } from 'd3-selection';
import {zoom} from 'd3-zoom'
import { writable } from 'svelte/store';


export function zooming(x,width, height) {

function zoomed (event) {
    console.log(event.transform.toString())
    console.log(select(x).attr("transform"))
    const t = event.transform
    select(x).attr("transform", event.transform.toString());
}

let z = zoom().on("zoom", zoomed).extent([[0,0],[width,height]])
select(x).call(z);
}
