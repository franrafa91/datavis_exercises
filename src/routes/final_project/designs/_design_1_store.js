import {writable} from "svelte/store"

export let builddom = writable();
export let maxenergy = writable();
export let data = writable();
export let scaling = writable();