import {writable} from "svelte/store"

export let mScale = writable()
export let dScale = writable()
export let cScale = writable()
export let eScale = writable()
